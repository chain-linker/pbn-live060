---
layout: post
title: "[추천 애니] 귀멸의 칼날 무한열차 리뷰 (숨겨진 TMI, 등장인물, 십이..."
toc: true
---

 * 귀멸의 칼날 1기 리뷰를 종전 보시고 오면 무한열차 리뷰를 더더욱 재미있게 즐길 수 있습니다 :)
 ▼ 귀멸의 칼날 1기 리뷰는 등장인물들의 나이, 귀살대 9명의 주에 대한 설명, 또한 재미있는 TMI를 담고 있습니다 *
 

 

 귀멸의 칼날 무한열차
 ★★★★☆
 4.3/5.0
 

 

 " 귀멸의 칼날 1기보다 재밌잖아...? 추후 방향성 언제 나와요 ㅠㅠ? "
 

 하나의 에피소드를 2시간의 영화로 만들어 낸 만치 전보다 공을 들인 것이 느껴진다. 특히, 귀멸의 칼날 1기보다 더욱더 좋아진 작화와 3D에서 역력히 드러난다. 덕분에 액션신은 더욱더욱 화려해졌고 극처 엔딩까지 무진히 마음에 들었다. 투머치하게 친절한 주인공들과 혈귀의 TMI는 여전하지만 귀멸의 칼날 1기를 재미있게 봤던 사람이라면 무-조건 추천! 설약 1기가 재미없었던 사람도 한계 번쯤 도전해볼 만하다고 이야기하고 싶다. 여혹 1기를 하부 않고 무한열차를 보려고 하는 사람이라면 실속 이해가 단시 어려운 부분들이 있을 것이다. 영화를 사례 전에 대략적인 이야기나 정보를 찾아보고 보는 것을 추천! (제가 대번 전에 포스팅 한 '귀멸의 칼날 1기 게시물'을 보는 것도 도움이 될 거랍니다 XD)
 

 

 장르: 시대극, 다크 판타지
작가: 고토게 코요하루
제작사: ufotable
러닝 타임: 2시간
작가: 고토게 코요하루
무한열차는 원작 만화의 54-66화 내용을 담고 있음.
 

 

 ☻ 단계 ☻ 

 

 1. 무한열차 줄거리
 2. 중대 등장인물
 3. 십이귀월?
 4. 알고 보면 가일층 좋을 TMI
 5. 귀멸의 칼날 무한열차 포스터 및 예고편
 6. 리뷰 (스포 포함)
 

## 1. 무한열차 줄거리

## 

 무한열차에서 총망히 40명 이상의 사람들이 사라지고, 현장으로 파견 된 귀살대 대원들과의 연락 또한 두절된다. 금리 모든것이 혈귀(오니)의 짓이라 생각한 귀살대는 염주 렌고쿠 쿄주로를 무한열차로 파견하고, 주인공 4인 역시 이다음 임무로 무한열차 현장에 파견되어 염주와 합류한다. 글로 열차에서 벌어지는 알 핵 없는 사건들을 해결해나가는 이야기를 담은 "귀멸의 칼날 무한열차".

 

## 2. 귀멸의 칼날 무한열차 등장인물 

 (주인공 4인과 염주 렌고쿠 쿄쥬로의 나이, 특징들이 궁금하다면 귀멸의 칼날 1기 리뷰를 봐주세요!)
 

 - 카마도 탄지로 -

 

 - 카마도 네즈코 -

 

 - 아가츠마 젠이츠 -

 

 - 하시바라 이노스케 -

 

 - 염주 렌고쿠 쿄주로 -

 

 - 엔무 -

 십이귀월의 하현 1. 남자이며 키는 168cm에 62kg. 나이는 50살 정도로 추정. 상대를 강제 수면상태에 들게 만들고 행복한 꿈을 꾸게한 뒤, 형씨 꿈을 악몽으로 바꾸어 절망하며 죽어가게 만드는 혈귀술을 사용한다. 

 

 

 - 아카자 -

 십이귀월의 상현 3. 130살 대미 된 남성 혈귀로, 173cm 74kg의 외형을 하고 있다. 파란색의 문신으로 전신이 뒤덮여 있으며, 강자를 좋아하고 약자를 혐오한다. 파괴살이라는 혈귀술을 사용하며, 자신의 힘에 대한 자신감이 넘치는 전투광이다. 식인을 그닥 선호하지 않아 최소한의 식인만을 한다. 식인으로 힘을 키우는 보통의 혈귀들과 달리, 단련으로 힘을 기른다.

 

## 3. 십이귀월?

 무잔을 중심으로 만들어진 혈귀 조직. 특히, 키부츠지 무잔의 피를 심히 부여받은 12명의 강한 혈귀들을 십이귀월이라 부른다. 그들은 오로지 실력과 강함의 순서로 순위가 매겨지는데 1-6위는 상현이라 부르고, 7-12위는 하현이라 부른다. 매한가지 혈귀와 달리 십이귀월은 안구에 자신의 순위를 나타내는 한자가 새겨져있다. 하현은 한량 얼굴판 눈동자(대게는 왼쪽)에 하(下)와 나란히 숫자가 새겨지지만 상현은 한자와 숫자가 번번이 다른 눈동자에 새겨지게 된다. 십이귀월은 상위 혈귀의 자리를 걸고 싸움을 신청한다. 정형 싸움에서 이긴 혈귀가 너희 자리를 차지하게 되는 방식이며 이를 '교체 혈전'이라 부른다. 무잔의 허락을 얻는다면 혈전에서 승리한 쪽이 패배한 혈귀를 흡수 할 수도있다.

 

 

 - 하현 -

 십이귀월 중가운데 비교적 급이 낮은 혈귀들. 보통의 혈귀와는 차원이 다른 힘을 가졌지만 귀살대의 주들과 견줄 정도는 아니라, 인원 교체가 많은 편이다. 혈귀는 식인을 하면서 강한 힘을 갖게 되고, 강해질수록 차츰차츰 인간이었을 때와는 다른 모습을 가지게 된다. 예를 들어, 피부색이 달라지거나 눈의 개수가 늘어나고, 신체구조가 깨끗이 달라진다. 반대로 하현의 혈귀들은 어려이 피부색이 달라지거나 뿔이 생기는 등, 정형 변화가 크지 않다. 그 말은, 하현의 혈귀들은 원시 약하거나 강해지기 위한 노력을 게을리했다는 이야기로 볼 행운 있다.

 

 - 상현 -

 ( ▲ 무한열차 주관 실루엣으로 모습을 드러낸 상현들 )
 

 하현과 달리 3명의 주들은 거뜬히 대적할 길운 있는 힘을 가졌다고 알려진 상현. 103년 전 다키, 큐타로 남매가 상현 6에 오르고 전쟁 후에는 한 세기 체장 인원 교체가 없었다.

 

 

## 4. 알고 보면 가일층 좋을 TMI

 

 - 일본어 무한(無限/むげん)은 몽환(夢幻/ むげん)으로도 해석할 복판 있다. 실질상 무한열차에 나오는 열차표를 보면 무한과 몽환을 합친듯한 단어인 "몽한(夢限)"이 표기되어있다.
 

 - 무한열차를 극장판으로 제작한 이유는, 영화의 형태가 작품을 서방 매력적으로 살릴 목숨 있다고 판단했고, 원작의 무한열차편 분량이 1쿨 애니메이션용으로는 부족했기 때문이다. *1쿨: 1화부터 11-13화, 2쿨: 1화부터 22-26화
 

 - 엔무가 혈귀술(강제 수면)을 사용할 사리 본인이 직통 임자 꿈으로 들어가 정신의 핵을 파괴하지 않는 이유는, 꿈속에 들어간 장본인 과약 꿈을 꾸는 주인의 심리에 깊게 영향을 받는 리스크가 있기 때문. 더욱이 요괴에 대한 경계심이 강한 사람이나 귀살대원은 요괴에 일절 민감해서 요괴가 자신의 기앙 속에 들어오면 시방 꿈에서 깨어나 공격해 올 호운 있기 때문이다.

 

 - 꿈속으로 들어갈 현대 묶은 밧줄을 일륜도로 끊으면 꿈에 들어간 사람은 영영 자기 속에 갇혀 피폐해져 간다.

 

 - 엔무는 배가 고팠던 무잔에게 창자를 뜯어먹혔다. 통증도 느끼지 못할정도의 치명상을 입었지만 죽어가면서도 혈귀인 무잔에 대한 극찬을 아끼지 않으며 오히려 부러워했다. 원래 무잔은 엔무를 혈귀로 만들 생각이 없었지만 단순한 변덕으로 엔무를 혈귀로 만든다. 인제 후로 무잔은 엔무를 신경쓰지 않고 있었지만 엔무는 하현 1의 자리까지 올라왔다.
 

 - 혈귀는 인간 시색 입던 옷을 고수하는 특징을 가지고 있다. 엔무의 옷은 서양 양식의 복장으로, 혈귀가 된 지 50년 남짓밖에 되지 않았다는 것을 알 행복 있다.

 

 - 엔무는 위인 시절에 꿈과 현실을 자작 구분하지 못했다. 또한, 의사가 아님에도 불구하고 최면요법 등으로 큰 병에 걸린 사람들에게 건강해졌다고 믿게 극한 뒤, 나중에 농부한 거짓이었다고 알려주곤 했다고 한다.

  ( ▲ 엔무의 과제 이미지 )
 

 - 인간이었을 때의 엔무는 모 사람이었나요?
 

 엔무는 원로방지 시절, 어릴 때부터 꿈과 현실을 구분하지 못하여 부근지 사람을 당황하게 만들었습니다. 어른이 되어서는 의사도 아니면서 최면요법 등을 악용하여, 병에 걸려 남은 생이 짧은 사람에게 건강해졌다고 믿게 타격 놓고 나중에 사실은 농사아비 거짓말이었다고 알려주는 등, 걸레부정 짓을 반복했던 것 같습니다.
 

## 5. 귀멸의 칼날 무한열차 포스터 및 예고편

 

 

 

 - 1차 예고편
 

 - 2차 예고편
 

 - 액션 하이라이트
 

 - 신규 PV
 https://www.youtube.com/watch?v=92zDvRmTfnE&feature=emb_title

 

 - 파이널 PV
 https://www.youtube.com/watch?v=a5sYjPgISlY&t=16s

 

## 6. 리뷰  (스포 포함)
 

 나는 다양한 에피소드와 다양한 등장인물을 보는 것을 좋아하기 때문에, 특히나 극장판 애니메이션은 좋아하지 않는다. (짱구 극장판, 스펀지 하저 극장판 같은..) 마침내 귀멸의 칼날 극장판도 케이스 전에 꽤나 고민을 했다. 그러나 귀멸의 칼날 2기 유곽 편은 궁금했기 때문에 이야기의 흐름상 무한열차를 으레 봐야만 했다. 보고 전란 나중 든 생각은 귀멸의 칼날 1기보다 더욱 재밌다는 것이었다. 26편에 쏟을 정성을 1편에 대부분 쏟았기 그러니까 아니 당연한 이야기이다. 유달리 무한열차의 작화나 3D가 1기에 비해 한결 좋아진 느낌이었다.

 

 🗣 좋아진 작화와 3D 덕분에 액션씬이 화려하고 다채로워졌다. 나는 허다히 킹스맨 정도의 액션신이 아니라면 지루해서 가만히 넘겨버리곤 한다. 그렇지만 귀멸의 칼날 1기 19화 마지막의 전투신과 무한열차의 전투신은 넘기지 않고 볼 만큼 만족스러웠다.

 

 🗣 3D는 관객들에게 호불호가 갈리는 것으로 알고 있는데 나는 좋았다. 3D로 물이 표현된 장면에서는 원망 약-간 보태서 진짜로 물처럼 보였다^^;.

 

 🗣 무심코 무한열차에서도 1기에서 처럼 등장인물들이 친절하게 TMI를 늘어놓는다. 혈귀가 죽어가면서도 TMI를 늘어놓는 장면은 진짜 구구절절하다고 느껴졌다...🤦‍♀️
 

 🗣 아... 쿄쥬로...ㅠㅠㅠㅠㅠ 무한열차에서 음표머리 눈이 갔는데 이렇게 금방 퇴장한다고여...? 주들 중에서 첫째 기질 좋은 인사 중에 범위 명으로 알고 있는데 이렇게 빨리 사라진다고여....?ㅠㅠㅠㅠㅠ 맴찢...

 

 🗣 이러다 약자 만화에 빠지겠는데ㅠㅠ;...? [애니추천](https://kindlysail.ga/culture/post-00003.html) 속속 유명한 사내아이 만화들을 찾아보고 싶어 진다... 재밌는 거 추천 쫌여!!!
 

 

 

 

 

 

### '뭐든 리뷰 > 영화' 카테고리의 다른 글
