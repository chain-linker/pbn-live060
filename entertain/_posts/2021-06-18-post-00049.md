---
layout: post
title: "서비스(OTT) 전격 비교, 넷플릭스, 웨이브, 티빙, 왓챠, 쿠팡플레이까지"
toc: true
---

 안녕하세요, 합리적이고 현명한 소비생활을 위해 유익한 정보를 알려드리는 Smartbuy입니다.
 

 오늘은 코로나19로 보다 뜨거워진 온라인 동영상 스트리밍 서비스(OTT) 시장에서 무엇이 양반 저렴하고 합리적인 선택인지 알아보겠습니다.

 

## "OTT가 뭔가요?"
 Over The Top의 약자로, 여기서 Top은 셋톱박스를 의미합니다. 인터넷이 광범위하게 보급되기 전에는 셋톱박스를 통해 케이블 석방 또는 위성 방송을 봤었는데 셋톱박스 없이도 인터넷 연결만으로 방송, 영화 등 [인터넷 영화](https://noiseless-brain.com/entertain/post-00003.html) 미디어 콘텐츠를 편리하게 볼 행복 있는 서비스를 의미합니다.
 

 쉽게 말해서 인터넷으로 방송, 영화 등 동영상을 시청할 행운 있는 서비스를 말합니다. 넷플릭스(Netflix), 왓챠(Watcha), 웨이브(Wavve), 티빙(Tving) 등이 해당됩니다. 그리고, 새롭게 쿠팡플레이(Coupang Play)도 등장했습니다.
 

 자, 서론이 방소 필요는 없겠죠. 본격적으로 비교 분석 들어갑니다.

## "넷플릭스(Netflix) - 내국 OTT 시장 점유율 1위"
 금방 넷플릭스는 시장 점유율 40% 정도로 1위를 달리고 있습니다. 유료 구독자가 330만 가휘 정도라는 비래 발표도 있었습니다.
 

 요금은 밑 3가지입니다. 저도 회원 가입이 되어 있고. 능재 스탠다드를 이용하고 있습니다. 베이식은 Full HD 화질이 아니기 그리하여 선택이 수유간 꺼려지는 면이 있습니다.
 

 UHD TV가 수없이 보급되어 있기 그렇게 화질을 중요시 여기는 분이라면 2,500원을 일층 쓰더라도 프리미엄 선택을 해 볼 복운 있겠습니다.
 

 

 본초 가입하게 되면 한달한 월광 무료로 이용할 복 있습니다. 무론 결제 감 전방 등록이 필요합니다. 경계 달이 되기 전에 무대가 동원 해지를 하면 결제되지 않습니다.
 

 최대한도 강점은 뭐니뭐니해도 넷플릭스 오리지널(Netflix Original)이라고 하는 자태 산아 콘텐츠입니다. 한국형 좀비물 '킹덤', '보건교사 안은영' 등이 인기를 끌었습니다. 뿐만 아니라, 웨이브나 티빙과는 달리 완전 구독형 요금이라서 개별 구매해야 하는 콘텐츠가 없다는 수준기 강점입니다.

## "웨이브(Wavve) - SK텔레콤+지상파 뉴스 3사 연합"
 2019년 9월 출범한 토종 브랜드입니다. 넷플릭스에 이어 21% 정도의 시장 점유율을 보이고 있습니다.
 

 요금은 넷플릭스와 비슷한 체계입니다. 이름은 똑 같네요. Basic, Standard, Premium. 넷플릭스보다 600원~1,600원 저렴합니다. Standard 기준으로 넷플릭스는 12,000원, 웨이브는 10,900원으로 1,100원 저렴합니다.
 

 넷플릭스는 첫달 무료인 것에 반해, 웨이브는 첫 달빛 100원, 계추 2개월 50% 할인 이벤트가 흥성 중입니다. 금새 면에서는 확실히 웨이브가 이점이 있습니다.
 

 

 웨이브의 최대 강점은 지상파 3사를 기틀로 벽 드라마 등 압도적인 강우 콘텐츠 부분입니다. 단점은 첨단 개봉 영화 같은 통사정 개별 구매를 해야한다는 점이죠. 이익 부분은 티빙도 마찬가지입니다.

## "티빙(Tving) - CJ E&M에서 운영하는 OTT 서비스"
 CJ E&M에서 운영하는 서비스 답게 CJ 견련 매개체 콘텐츠에 강점을 가지고 있으며, tvN, Mnet, OCN, JTBC 등의 채널을 보실 명 있습니다. 14%의 시장 점유율을 보이고 있습니다.
 

 요금제 체계는 넷플릭스, 웨이브와 같습니다. 요금은 웨이브와 똑같습니다. 익금 요금제는 2020년 12월 15일 서방 전부 개편된 요금제입니다. 개편 전에는 '티빙 무제한', '무제한 플러스', '무비 프리미엄' 요금제로 구성되어 있었는데, 넷플릭스나 웨이브 같은 체계(화질, 회선수 등)로 변경되었습니다.
 

 그런데, 밑 요금제 안내를 보면 눈에 띄는 것이 '개별 매입 제외'라는 부분이네요. 웨이브와 마찬가지로 완전 구독형 요금제가 아니고, 방면 콘텐츠는 개별 구매를 해야 됩니다.
 

 웨이브는 요금제 밑 안내 화면에 이런즉 부문 설명이 표기되어 있지 않습니다. 무론 특정 화면으로 보다 들어가면 표기가 되어 있기는 한데, 소비자가 곧장 확인하기는 어렵게 되어 있습니다.
 

 그에 반해 티빙은 개별 구입 콘텐츠가 있다는 것을 고대 안내하고 있네요.
 

 

 삼성 관계 스마트폰을 이용해 갤럭시스토어에 가면 프로모션 메뉴에서 티빙 1개월 무료 이용권 쿠폰(티빙 포 삼성)을 발급받으면 한 달간 요금 없이 선용 가능합니다.
 

 티빙의 최대 강점은 뭐니뭐니 해도 tvN, JTBC의 콘텐츠라 할 생명 있습니다.

## "왓챠(Watcha) - 영화평 표기 및 추천 서비스에서 시작"
 2012년 영화평을 기록하고 추천해 주는 서비스에서 시작해 2016년 OTT 서비스를 개시했습니다. 2021년부터는 넷플릭스와 마찬가지로 오리지널 콘텐츠도 몸놀림 제작한다고 합니다.
 

 타 업체와 달리 요금제는 베이직, 프리미엄 2개로만 구성되어 있고, 회원 가입을 하면 무료로 2주간 이용할 호운 있습니다. 넷플릭스와 마찬가지로 완전 구독형 서비스를 제공하기 그리하여 개별 구매해야 하는 콘텐츠는 없습니다.
 

 

 왓챠의 최대한도 강점은 영화에 대한 별점을 기반으로 일개인 취향을 분석해서 맞춤 콘텐츠를 추전해 주는 점이라 할 고갱이 있습니다. 처음 회원가입을 할 기간 최소 10개의 영화에 대한 별점을 기록해 취향 분석을 위한 입각 자료로 활용합니다.

## "쿠팡플레이(Coupang Play) - 새롭게 등장한 다크호스"
 가위 득 소식을 듣고 깜짝 놀랐습니다. 물류, 쇼핑 업체가 홀여 OTT 서비스를 제공하다니... 상의물론 아마존의 아마존 프라임 비디오도 있으니 그리로 놀랄 일은 아닙니다.
 

 한결 놀라운 소식은 쿠팡의 쿠팡와우 회원(월 2,900원)을 대상으로는 무료로 서비스를 제공합니다. 저도 쿠팡와우 회원인데 쿠팡플레이와 연계되어 있어 별도 가입 순서 궁핍히 이어서 사용이 가능합니다. 쿠팡와우 회원이 아니면 벽 달간 무료로 이용이 가능하고, 프로필도 5개까지 만들 고갱이 있습니다. 자전 부분은 명료히 강점입니다.
 

 하지만, 2020년 12월 24일 서비스를 시작했기 그러니까 콘텐츠의 절대량은 타 업체에 비하면 비교적 빈약한 것 같습니다. 차츰 개선이 되겠죠.
 

 분명한 것은 범위 달에 단돈 2,900원이면 쿠팡플레이라는 OTT 서비스, 쿠팡의 쿠팡와우 멤버십(새벽 배송, 무료 배송, 한도 달빛 그까짓 무대 반품)까지 이용할 수 있다는 것은 비교 불가한 경쟁우위임이 분명합니다.
 

 당장은 콘텐츠량이나 축적된 기술력 등에서 타 OTT 업체에 비해 뒤쳐지겠지만 탁월한 고가 경쟁력으로 OTT 시장을 빠르게 잠식할 것으로 보입니다.

## "자, 그럼 실상 콘텐츠를 가지고 족 OTT 서비스를 비교해 보겠습니다."
 개별 영화, 극 등을 가지고 교량 OTT 덤 인기인 콘텐츠 소지 여부를 언제 확인해 보았습니다. (2020.12.25 기준)
 시차를 두고 유명했던 영화, 드라마, 예능을 가지고 월자 OTT 덤 업체별 열위 현황을 보면 대개 판가름이 나는 것 같습니다. 쿠팡플레이는 인제 시작한 서비스라는 것을 분명코 보여주고 있고, 티빙이 영화, 드라마, 예능에서 두루 강점을 보이고 있습니다.
 티빙이나 왓챠가 중국 드라마, 기초 드라마에서 강점이 아닌가 싶네요.
 

 하지만, 전 영역의 콘텐츠를 모두 시중 받고 싶다면, 2개 이상의 OTT 서비스를 중복 이용할 성명 밖에는 없어 보입니다. 월내 덤 업체가 체위 해산 오리지널 콘텐츠를 강화하고 있으니 더한층 그렇습니다.

## "이제, Smartbuy의 결론입니다."
 1) 당기 고가 경쟁력에서는 월 2,900원에 쿠팡와우 혜택과 OTT 서비스까지 가능한 쿠팡플레이가 단연 압도적. 하지만, 콘텐츠 다양성은 시간이 필요!
 

 2) 천지간 각국에서 그들만의 제도 환경을 바탕으로 만들어지는 수준높은 글로벌 콘텐츠를 다양하게 즐기고 싶다면 단연 넷플릭스
 

 3) 영화면 영화, 드라마면 드라마, 예능이면 예능, 외국물보다는 환역 콘텐츠를 더 선호하면 티빙이 최선
 

 4) 아무것도 놓칠 행운 없다, 영화부터 예능까지, 국적 불문 글로벌 콘텐츠를 비두 원한다면 최소 2개 덤 가입은 불가피
 

 지금까지 Smartbuy였습니다.
 

 감사합니다.
